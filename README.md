### Fast configuration 
![输入图片说明](https://images.gitee.com/uploads/images/2021/1107/172306_35780a94_5705291.png "屏幕截图.png")
- Appearance, fast code alignment
- Shared clipboard，tab indent，fast page split
- File tree，debugging mode supported

![](https://img.shields.io/badge/zhbvim-v1.0.0-blueviolet)

---
#### Linux
```bash
cd ~
mkdir .vim
cd .vim
git clone https://gitee.com/HangbinZheng/zhbvim.git
mv zhbvim/vimrc .
```

#### Windows

```
cd ~/vimfiles
git clone https://gitee.com/HangbinZheng/zhbvim.git
move zhbvim/win_vimrc .
```

#### MacOS

```bash
cd ~/.vim
git clone https://gitee.com/HangbinZheng/zhbvim.git
mv zhbvim/vimrc .
```

#### PlugInstall
```bash
# 插件管理器
cp -rf zhbvim/autoload ~/.vim/

# 插件下载
vim ./vimrc
:PlugInstall

# 代码调试
wget https://repo.anaconda.com/archive/Anaconda3-2020.07-Linux-x86_64.sh
cd ~/.vim/plugged/vimspector
python install_gadget.py --enable-python

# 文件管理器
ff <Enter>

# 代码对齐
tt/'对齐用的分隔符'
```
> 探索更多插件请访问 [vimawesome](https://vimawesome.com/)
