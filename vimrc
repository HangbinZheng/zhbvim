syntax on
set wrap
set noeb
set number
set mouse=a
set showcmd 
set wildmenu
set autochdir
set cursorline
set ignorecase
set history=500
set nocompatible
set encoding=utf-8
let mapleader = " " 
:colorscheme koehler
set termencoding=utf-8
set clipboard=unnamedplus
set backspace=indent,eol,start
set fileencodings=utf-8,ucs-bom,gb18030,gbk,gb2312,cp936

set hlsearch
exec "nohlsearch"
set incsearch
noremap <LEADER><CR> :nohlsearch<CR>

map sd :set splitright<CR>:vsplit<CR>
map sa :set nosplitright<CR>:vsplit<CR>
map sw :set nosplitbelow<CR>:split<CR>
map ss :set splitbelow<CR>:split<CR>

map <LEADER><Left>  <c-w>h
map <LEADER><down>  <c-w>j
map <LEADER><up>    <c-w>k
map <LEADER><right> <c-w>l

noremap = nzz
noremap - Nzz

noremap <c-l> $
noremap <c-h> 0
noremap <c-k> gg
noremap <c-j> G

map wq :wq!
map z :q!
map ggg G

map ta :tabe<CR>
map tr :-tabnext<CR>
map rr :+tabnext<CR>
map s <nop>
map n <nop>

set showmatch
set tabstop=4
set shiftwidth=4
set autoindent

autocmd BufWritePost $MYVIMRC source $MYVIMRC

filetype plugin on
map <F5> :call PRUN()<CR>
func! PRUN()
    exec "w" 
	    if &filetype == 'python'
		        exec "!python %"
				    endif
					endfunc

set gcr=n-v-c:ver25-Cursor/lCursor,ve:ver35-Cursor,o:hor50-Cursor,i-ci:ver25-Cursor/lCursor

inoremap ( ()<ESC>i
inoremap ) <c-r>=ClosePair(')')<CR>
inoremap { {}<ESC>i
inoremap } <c-r>=ClosePair('}')<CR>
inoremap [ []<ESC>i
inoremap ] <c-r>=ClosePair(']')<CR>
inoremap ' ''<ESC>i
inoremap " ""<ESC>i
function ClosePair(char)
 if getline('.')[col('.') - 1] == a:char
   return "\<Right>"
    else
	  return a:char
	   endif
	   endf

" plugin config
call plug#begin('~/.vim/plugged')
Plug 'https://gitee.com/HangbinZheng/nerdtree.git'
Plug 'https://gitee.com/HangbinZheng/vim-surround.git'
Plug 'https://gitee.com/HangbinZheng/wildfire.vim'
Plug 'https://gitee.com/HangbinZheng/vimspector.git'
Plug 'https://gitee.com/HangbinZheng/YouCompleteMe.git'
Plug 'https://gitee.com/HangbinZheng/rainbow.git'
Plug 'https://gitee.com/HangbinZheng/lightline.vim'
Plug 'https://gitee.com/HangbinZheng/tabular.git'
call plug#end()

" file tree
noremap ff :NERDTreeToggle

" wildfire + vim surround
map <SPACE> <Plug>(wildfire-fuel)
map " S"
map { S{
map ( S(
map	' S'

" ranbow
let g:rainbow_active = 1

" lightline
set laststatus=2
if !has('gui_running')
  set t_Co=256
endif
set noshowmode

" Tabular
noremap tt :Tabular
